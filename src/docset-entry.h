/*
 * SPDX-FileCopyrightText: 2020 Ayman Bagabas <ayman.bagabas@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef __DOCSET_ENTRY_H__
#define __DOCSET_ENTRY_H__

#include <glib-object.h>
#include "docset-entry-type.h"
#include "docset-enum-types.h"

G_BEGIN_DECLS

typedef struct _DocsetEntry DocsetEntry;

#define DOCSET_TYPE_ENTRY (docset_entry_get_type ())

struct _DocsetEntry {
    gint id;
    gchar *name;
    const DocsetEntryType *type;
    gchar *path;
};

GType                  docset_entry_get_type               (void);

DocsetEntry           *docset_entry_new                    (gint id,
                                                            const gchar *name,
                                                            const gchar *type,
                                                            const gchar *path);

DocsetEntry           *docset_entry_copy                   (DocsetEntry *self);

void                   docset_entry_free                   (DocsetEntry *self);

const gchar           *docset_entry_get_name               (DocsetEntry *self);

const gchar           *docset_entry_get_path               (DocsetEntry *self);

const DocsetEntryType *docset_entry_get_entry_type         (DocsetEntry *self);

DocsetEntryTypeId      docset_entry_get_type_id            (DocsetEntry *self);

const gchar           *docset_entry_get_type_name          (DocsetEntry *self);

G_END_DECLS

#endif /* #ifndef __DOCSET_ENTRY_H__ */
