/*
 * SPDX-FileCopyrightText: 2020 Ayman Bagabas <ayman.bagabas@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "docset-file.h"
#include "docset-cursor.h"
#include <libxml/parser.h>

/**
 * SECTION: docset-file
 * @title: DocsetFile
 * @short_description: TODO
 *
 * TODO
 */

/**
 * DocsetFile:
 *
 * Data structure representing a #DocsetFile.
 *
 * Since: UNRELEASED
 */

/**
 * DocsetFileClass:
 *
 * The class of a #DocsetFile.
 *
 * Since: UNRELEASED
 */

enum {
    PROP_0,
    N_PROPS
};

enum {
    SIGNAL_0,
    N_SIGNALS
};

struct _DocsetFilePrivate {
    GFile *root;
    GFile *db_file;
    GFile *props_file;
    DocsetCursor *cursor;
    GHashTable *props;
};

G_DEFINE_TYPE_WITH_PRIVATE (DocsetFile, docset_file, G_TYPE_OBJECT)

static GParamSpec *obj_properties[N_PROPS] = { NULL };

static guint signals[N_SIGNALS] = { 0 };

const gchar  *
docset_file_get_props_bundle_id (DocsetFile *self)
{
    g_return_val_if_fail (DOCSET_IS_FILE(self), NULL);

    DocsetFilePrivate *priv;

    priv = docset_file_get_instance_private (self);

    return g_hash_table_lookup(priv->props, "CFBundleIdentifier");
}

const gchar  *
docset_file_get_props_bundle_name (DocsetFile *self)
{
    g_return_val_if_fail (DOCSET_IS_FILE(self), NULL);

    DocsetFilePrivate *priv;

    priv = docset_file_get_instance_private (self);

    return g_hash_table_lookup(priv->props, "CFBundleName");
}

gboolean
docset_file_get_props_is_dash (DocsetFile *self)
{
    g_return_val_if_fail (DOCSET_IS_FILE(self), FALSE);

    DocsetFilePrivate *priv;

    priv = docset_file_get_instance_private (self);

    return g_strcmp0(g_hash_table_lookup(priv->props, "isDashDocset"), "1") == 0;
}

gboolean
docset_file_get_props_is_javascript_enabled (DocsetFile *self)
{
    g_return_val_if_fail (DOCSET_IS_FILE(self), FALSE);

    DocsetFilePrivate *priv;

    priv = docset_file_get_instance_private (self);

    return g_strcmp0(g_hash_table_lookup(priv->props, "isJavaScriptEnabled"), "1") == 0;
}

const gchar  *
docset_file_get_props_platform_family (DocsetFile *self)
{
    g_return_val_if_fail (DOCSET_IS_FILE(self), NULL);

    DocsetFilePrivate *priv;

    priv = docset_file_get_instance_private (self);

    return g_hash_table_lookup(priv->props, "DocSetPlatformFamily");
}

const gchar  *
docset_file_get_props_index_filepath (DocsetFile *self)
{
    g_return_val_if_fail (DOCSET_IS_FILE(self), NULL);

    DocsetFilePrivate *priv;
    gchar *file_path;

    priv = docset_file_get_instance_private (self);
    file_path = g_hash_table_lookup(priv->props, "dashIndexFilePath");
    if (file_path == NULL) {
        gchar *root_path = g_file_get_path(priv->root);
        gchar *index_path = g_strconcat(root_path,
                                        DOCSET_FILE_INDEX_PATH,
                                        NULL);
        GFile *index_file = g_file_new_for_path(index_path);
        if (g_file_query_exists(index_file, NULL))
            file_path = "index.html";

        g_clear_object (&index_file);
        g_free (index_path);
        g_free (root_path);
    }

    return file_path;
}

gchar *
docset_file_get_root_path (DocsetFile *self)
{
    g_return_val_if_fail (DOCSET_IS_FILE(self), NULL);

    DocsetFilePrivate *priv;

    priv = docset_file_get_instance_private (self);

    return g_file_get_path(priv->root);
}

gchar *
docset_file_get_document_path (DocsetFile *self)
{
    g_return_val_if_fail (DOCSET_IS_FILE(self), NULL);

    gchar *document_path;
    gchar *root_path = NULL;

    root_path = docset_file_get_root_path(self);
    document_path = g_strconcat(root_path, DOCSET_FILE_DOCUMENT_PATH, NULL);

    g_free (root_path);

    return document_path;
}

gchar *
docset_file_get_index_path (DocsetFile *self)
{
    g_return_val_if_fail (DOCSET_IS_FILE(self), NULL);

    gchar *index_path;
    gchar *document_path = NULL;

    document_path = docset_file_get_document_path(self);
    index_path = (gchar *)docset_file_get_props_index_filepath(self);
    if (index_path != NULL) {
        index_path = g_strconcat(document_path,
                                 "/",
                                 index_path,
                                 NULL);
    }

    g_free (document_path);

    return index_path;
}

/**
 *
 * return: (no transfer) #DocsetCursor *
 */
DocsetCursor *
docset_file_get_cursor (DocsetFile *self)
{
    g_return_val_if_fail (DOCSET_IS_FILE(self), NULL);

    DocsetFilePrivate *priv = docset_file_get_instance_private (self);

    return priv->cursor;
}

GFile *
open_file_for_path (const gchar *path)
{
    g_return_val_if_fail(path != NULL, NULL);

    GFile *file;

    file = g_file_new_for_path (path);
    if (!g_file_query_exists (file, NULL)) {
        g_warning ("Failed to open '%s', file does not exist.\n", path);
        g_object_unref (file);
        return NULL;
    }

    return file;
}

GFile *
open_root_file_for_path (const gchar *path)
{
    g_return_val_if_fail(path != NULL, NULL);

    GFile *file = NULL;

    file = open_file_for_path (path);
    if (!G_IS_FILE (file))
        goto fail;

    if (g_file_query_file_type (file, G_FILE_QUERY_INFO_NONE, NULL)
            != G_FILE_TYPE_DIRECTORY) {
        g_warning ("Failed to open '%s', file is not a directory.\n", path);
        goto fail;
    }

    if (!g_str_has_suffix (path, ".docset")) {
        g_warning (
            "Failed to open '%s', file does not have a '.docset' format.\n",
            path);
        goto fail;
    }

    return file;

fail:
    g_object_unref (file);
    return NULL;
}

GFile *
open_db_file_for_path (const gchar *path)
{
    g_return_val_if_fail(path != NULL, NULL);

    GFile *db_file = NULL;
    gchar *db_path = NULL;

    db_path = g_strconcat(path, DOCSET_FILE_DB_PATH, NULL);
    db_file = open_file_for_path (db_path);
    if (!G_IS_FILE(db_file))
        goto fail;

    if (g_file_query_file_type(db_file, G_FILE_QUERY_INFO_NONE, NULL)
            != G_FILE_TYPE_REGULAR) {
        g_warning("Failed to open database '%s', not a regular file.", db_path);
        goto fail;
    }

    g_free (db_path);
    return db_file;

fail:
    g_free (db_path);
    g_object_unref (db_file);
    return NULL;
}

GFile *
open_props_file_for_path (const gchar *path)
{
    g_return_val_if_fail (path != NULL, NULL);

    GFile *props_file = NULL;
    gchar *props_path = NULL;

    props_path = g_strconcat(path, DOCSET_FILE_PLIST_PATH, NULL);
    props_file = open_file_for_path (props_path);
    if (!G_IS_FILE(props_file))
        goto fail;

    if (g_file_query_file_type(props_file, G_FILE_QUERY_INFO_NONE, NULL)
            != G_FILE_TYPE_REGULAR) {
        g_warning("Failed to open database '%s', not a regular file.", props_path);
        goto fail;
    }

    g_free (props_path);
    return props_file;

fail:
    g_free (props_path);
    g_object_unref (props_file);
    return NULL;
}

xmlNode *
parse_props_get_dict_node (xmlNode *tree)
{
    xmlNode *node;
    if (tree && xmlStrcmp(tree->name, (const xmlChar *)"plist") == 0) {
        for (node = tree->children; node; node = node->next) {
            if (node->type == XML_ELEMENT_NODE) {
                if (xmlStrcmp(node->name, (const xmlChar *)"dict") == 0) {
                    return node->children;
                }
            }
        }
    }

    return NULL;
}

GHashTable *
parse_props_file (GFile *file)
{
    g_return_val_if_fail (G_IS_FILE(file), NULL);

    GHashTable *props = NULL;
    xmlDocPtr doc = NULL;
    xmlNode *root = NULL;
    xmlNode *dict = NULL;
    gchar *file_path = NULL;
    gchar *key = NULL;
    gchar *value = NULL;

    file_path = g_file_get_path(file);
    if (!file_path) {
        g_warning("Failed to get props file path.\n");
        goto clean;
    }

    props = g_hash_table_new_full(g_str_hash, g_str_equal, g_free, g_free);
    doc = xmlParseFile(file_path);
    if (!doc) {
        g_warning("Failed to parse build props tree.\n");
        props = NULL;
        goto clean;
    }

    root = xmlDocGetRootElement(doc);
    if (!root) {
        g_warning("Failed to get props document root element.\n");
        g_object_unref (props);
        props = NULL;
        goto clean;
    }

    xmlChar *content;
    for (dict = parse_props_get_dict_node(root); dict; dict = dict->next) {
        if (dict->type == XML_ELEMENT_NODE) {
            if (xmlStrcmp(dict->name, (const xmlChar *)"key") == 0) {
                content = xmlNodeGetContent(dict);
                key = g_strdup((gchar *)content);
                xmlFree (content);
                continue;
            }

            if (xmlStrcmp(dict->name, (const xmlChar *)"string") == 0) {
                content = xmlNodeGetContent(dict);
                value = g_strdup((gchar *)content);
                xmlFree (content);
            }

            if (xmlStrcmp(dict->name, (const xmlChar *)"true") == 0 ||
                xmlStrcmp(dict->name, (const xmlChar *)"false") == 0) {
                value = g_strdup_printf("%d", xmlStrcmp(dict->name, (const xmlChar *)"true") == 0);
            }

            if (key && value) {
                if (!g_hash_table_insert(props, key, value))
                    g_warning("Duplicate key while parsing docset props: %s.", key);
                key = value = NULL;
            } else {
                g_free (key);
                g_free (value);
            }
        }
    }

clean:
    xmlFreeDoc (doc);
    xmlCleanupParser ();
    g_free (file_path);
    return props;
}

GHashTable *
docset_file_get_props (DocsetFile *self)
{
    g_return_val_if_fail (DOCSET_IS_FILE(self), NULL);

    DocsetFilePrivate *priv = docset_file_get_instance_private (self);

    return priv->props;
}

DocsetFile *
docset_file_new (GFile *file)
{
    g_return_val_if_fail(G_IS_FILE(file), NULL);

    gchar *file_path;
    DocsetFile *self;

    file_path = g_file_get_path(file);
    self = docset_file_new_for_path(file_path);
    g_free (file_path);

    return self;
}

DocsetFile *
docset_file_new_for_path (const gchar *path)
{
    g_return_val_if_fail (path != NULL, NULL);

    DocsetFile *self;
    DocsetFilePrivate *priv;
    GFile *root = NULL;
    GFile *db_file = NULL;
    GFile *props_file = NULL;
    GHashTable *props = NULL;
    DocsetCursor *cursor = NULL;

    self = g_object_new (DOCSET_TYPE_FILE, NULL);
    priv = docset_file_get_instance_private (self);

    root = open_root_file_for_path (path);
    if (!G_IS_FILE (root)) {
        g_clear_object (&self);
        goto exit;
    }

    db_file = open_db_file_for_path (path);
    if (!G_IS_FILE (db_file)) {
        g_clear_object (&self);
        goto exit;
    }

    props_file = open_props_file_for_path (path);
    if (!G_IS_FILE (props_file)) {
        g_clear_object (&self);
        goto exit;
    }

    props = parse_props_file (props_file);
    if (props == NULL) {
        g_clear_object (&self);
        goto exit;
    }

    cursor = docset_cursor_new_for_file (db_file);
    if (!DOCSET_IS_CURSOR(cursor)) {
        g_clear_object (&self);
        goto exit;
    }

    priv->root = g_object_ref (root);
    priv->db_file = g_object_ref (db_file);
    priv->props_file = g_object_ref (props_file);
    priv->cursor = g_object_ref (cursor);
    priv->props = g_hash_table_ref (props);

exit:
    g_object_unref (cursor);
    g_hash_table_unref (props);
    g_object_unref (props_file);
    g_object_unref (db_file);
    g_object_unref (root);
    return self;
}

static void
docset_file_get_property (GObject *object,
                          guint property_id,
                          GValue *value,
                          GParamSpec *pspec)
{
    DocsetFile *self = DOCSET_FILE (object);
    DocsetFilePrivate *priv = docset_file_get_instance_private (self);

    switch (property_id) {
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        break;
    }
}

static void
docset_file_set_property (GObject *object,
                          guint property_id,
                          const GValue *value,
                          GParamSpec *pspec)
{
    DocsetFile *self = DOCSET_FILE (object);
    DocsetFilePrivate *priv = docset_file_get_instance_private (self);

    switch (property_id) {
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        break;
    }
}

static void
docset_file_dispose (GObject *object)
{
    DocsetFile *self = DOCSET_FILE (object);
    DocsetFilePrivate *priv = docset_file_get_instance_private (self);

    g_clear_object (&priv->root);
    g_clear_object (&priv->db_file);
    g_clear_object (&priv->props_file);
    g_clear_object (&priv->cursor);
    g_hash_table_remove_all(priv->props);
    g_clear_pointer(&priv->props, g_hash_table_unref);

    G_OBJECT_CLASS (docset_file_parent_class)->dispose (object);
}

static void
docset_file_finalize (GObject *object)
{
    DocsetFile *self = DOCSET_FILE (object);
    DocsetFilePrivate *priv = docset_file_get_instance_private (self);

    G_OBJECT_CLASS (docset_file_parent_class)->finalize (object);
}

static void
docset_file_constructed (GObject *object)
{
    DocsetFile *self = DOCSET_FILE (object);

    G_OBJECT_CLASS (docset_file_parent_class)->constructed (object);
}

static void
docset_file_class_init (DocsetFileClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);

    object_class->get_property = docset_file_get_property;
    object_class->set_property = docset_file_set_property;
    object_class->constructed = docset_file_constructed;
    object_class->dispose = docset_file_dispose;
    object_class->finalize = docset_file_finalize;

    /* g_object_class_install_properties (object_class, N_PROPS,
     * obj_properties); */
}

static void
docset_file_init (DocsetFile *self)
{
    self->priv = docset_file_get_instance_private (self);
}
