/*
 * SPDX-FileCopyrightText: 2020 Ayman Bagabas <ayman.bagabas@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef __DOCSET_FILE_H__
#define __DOCSET_FILE_H__

#include <gio/gio.h>
#include <glib-object.h>
#include "docset-cursor.h"

G_BEGIN_DECLS

typedef struct _DocsetFile DocsetFile;
typedef struct _DocsetFileClass DocsetFileClass;
typedef struct _DocsetFilePrivate DocsetFilePrivate;
typedef struct _DocsetFileProps DocsetFileProps;

#define DOCSET_TYPE_FILE (docset_file_get_type ())
#define DOCSET_FILE(obj)                                                      \
    (G_TYPE_CHECK_INSTANCE_CAST ((obj), DOCSET_TYPE_FILE, DocsetFile))
#define DOCSET_FILE_CLASS(klass)                                              \
    (G_TYPE_CHECK_CLASS_CAST ((klass), DOCSET_TYPE_FILE, DocsetFileClass))
#define DOCSET_IS_FILE(obj)                                                   \
    (G_TYPE_CHECK_INSTANCE_TYPE ((obj), DOCSET_TYPE_FILE))
#define DOCSET_IS_FILE_CLASS(klass)                                           \
    (G_TYPE_CHECK_CLASS_TYPE ((klass), DOCSET_TYPE_FILE))
#define DOCSET_FILE_GET_CLASS(obj)                                            \
    (G_TYPE_INSTANCE_GET_CLASS ((obj), DOCSET_TYPE_FILE, DocsetFileClass))

#define DOCSET_FILE_CONTENTS_PATH  "/Contents"
#define DOCSET_FILE_RESOURCES_PATH DOCSET_FILE_CONTENTS_PATH"/Resources"
#define DOCSET_FILE_DB_PATH        DOCSET_FILE_RESOURCES_PATH"/docSet.dsidx"
#define DOCSET_FILE_PLIST_PATH     DOCSET_FILE_CONTENTS_PATH"/Info.plist"
#define DOCSET_FILE_DOCUMENT_PATH  DOCSET_FILE_RESOURCES_PATH"/Documents"
#define DOCSET_FILE_INDEX_PATH     DOCSET_FILE_DOCUMENT_PATH"/index.html"

struct _DocsetFileClass {
    GObjectClass parent_class;

    /* Padding for future expansion */
    gpointer padding[12];
};

struct _DocsetFile {
    GObject parent_instance;

    DocsetFilePrivate *priv;
};

GType         docset_file_get_type                        (void);

DocsetFile   *docset_file_new                             (GFile *file);

DocsetFile   *docset_file_new_for_path                    (const gchar *path);

gchar        *docset_file_get_root_path                   (DocsetFile *self);

gchar        *docset_file_get_document_path               (DocsetFile *self);

gchar        *docset_file_get_index_path                  (DocsetFile *self);

DocsetCursor *docset_file_get_cursor                      (DocsetFile *self);

GHashTable   *docset_file_get_props                       (DocsetFile *self);

const gchar  *docset_file_get_props_bundle_id             (DocsetFile *self);

const gchar  *docset_file_get_props_bundle_name           (DocsetFile *self);

gboolean      docset_file_get_props_is_dash               (DocsetFile *self);

gboolean      docset_file_get_props_is_javascript_enabled (DocsetFile *self);

const gchar  *docset_file_get_props_platform_family       (DocsetFile *self);

const gchar  *docset_file_get_props_index_filepath        (DocsetFile *self);

G_END_DECLS

#endif /* #ifndef __DOCSET_FILE_H__*/
