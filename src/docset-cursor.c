/*
 * SPDX-FileCopyrightText: 2020 Ayman Bagabas <ayman.bagabas@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "docset-cursor.h"
#include "docset-file.h"
#include <sqlite3.h>

/**
 * SECTION: docset-cursor
 * @title: DocsetCursor
 * @short_description: TODO
 *
 * TODO
 */

/**
 * DocsetCursor:
 *
 * Data structure representing a #DocsetCursor.
 *
 * Since: UNRELEASED
 */

/**
 * DocsetCursorClass:
 *
 * The class of a #DocsetCursor.
 *
 * Since: UNRELEASED
 */

#define DASH_BASE_QUERY                                                       \
    "select id, name, type, path from searchIndex where "     \
    "name like ? order by id"

#define DASH_COUNT_QUERY                                                      \
    "select count(*) from searchIndex"

enum { PROP_0, N_PROPS };

enum { SIGNAL_0, N_SIGNALS };

struct _DocsetCursorPrivate {
    GFile *file;
    sqlite3 *db;
    sqlite3_stmt *stmt;
};

G_DEFINE_TYPE_WITH_PRIVATE (DocsetCursor, docset_cursor, G_TYPE_OBJECT)

static GParamSpec *obj_properties[N_PROPS] = {
    NULL,
};

static guint signals[N_SIGNALS] = { 0 };

GList *
docset_cursor_get_entries (DocsetCursor *self)
{
    g_return_val_if_fail (DOCSET_IS_CURSOR(self), NULL);

    return docset_cursor_vfind(self, "");
}

gint
docset_cursor_count_entries(DocsetCursor *self)
{
    g_return_val_if_fail (DOCSET_IS_CURSOR(self), -1);

    DocsetCursorPrivate *priv;
    gint err = 0;

    priv = docset_cursor_get_instance_private (self);
    err = sqlite3_finalize(priv->stmt);
    if (err != SQLITE_OK)
        g_warning ("Clearing database statement failed %d %s\n",
                err,
                sqlite3_errstr (err));

    err = sqlite3_prepare_v2 (
        priv->db, DASH_COUNT_QUERY, -1, &priv->stmt, NULL);

    err = sqlite3_step (priv->stmt);
    if (err != SQLITE_ROW) {
        if (err != SQLITE_DONE)
            g_warning ("Step failed %d %s\n", err, sqlite3_errstr (err));

        err = sqlite3_reset (priv->stmt);
        if (err != SQLITE_OK)
            g_warning ("Clearing database statement failed %d %s\n",
                       err,
                       sqlite3_errstr (err));

        return -err;
    }

    return sqlite3_column_int(priv->stmt, 0);
}

DocsetEntry *
docset_cursor_find (DocsetCursor *self, const gchar *pattern)
{
    g_return_val_if_fail (DOCSET_IS_CURSOR(self), NULL);
    g_return_val_if_fail(pattern != NULL, NULL);

    DocsetCursorPrivate *priv;
    gint err = 0;
    gchar *find_pattern = NULL;

    priv = docset_cursor_get_instance_private (self);
    err = sqlite3_finalize(priv->stmt);
    if (err != SQLITE_OK)
        g_warning ("Clearing database statement failed %d %s\n",
                err,
                sqlite3_errstr (err));

    err = sqlite3_prepare_v2 (
        priv->db, DASH_BASE_QUERY, -1, &priv->stmt, NULL);
    if (err != SQLITE_OK) {
        g_warning ("Prepare failed %s\n", sqlite3_errstr (err));
        return NULL;
    }

    find_pattern = g_strdup_printf("%%%s%%", pattern);
    err = sqlite3_bind_text (priv->stmt, 1, find_pattern, -1, SQLITE_TRANSIENT);
    if (err != SQLITE_OK) {
        g_warning ("Bind failed %s\n", sqlite3_errstr (err));
        g_free (find_pattern);
        return NULL;
    }

    g_free (find_pattern);
    return docset_cursor_next (self);
}

DocsetEntry *
docset_cursor_next (DocsetCursor *self)
{
    g_return_val_if_fail (DOCSET_IS_CURSOR(self), NULL);

    DocsetCursorPrivate *priv;
    gint err = 0;

    priv = docset_cursor_get_instance_private (self);
    err = sqlite3_step (priv->stmt);
    if (err != SQLITE_ROW) {
        if (err != SQLITE_DONE)
            g_warning ("Step failed %d %s\n", err, sqlite3_errstr (err));

        err = sqlite3_reset (priv->stmt);
        if (err != SQLITE_OK)
            g_warning ("Clearing database statement failed %d %s\n",
                       err,
                       sqlite3_errstr (err));

        return NULL;
    }

    return docset_entry_new (sqlite3_column_int (priv->stmt, 0),
                            (const gchar *)sqlite3_column_text (priv->stmt, 1),
                            (const gchar *)sqlite3_column_text (priv->stmt, 2),
                            (const gchar *)sqlite3_column_text (priv->stmt, 3));
}

/**
 * Returns: must be freed
 */
GList *
docset_cursor_vfind (DocsetCursor *self,
                     const gchar *pattern)
{
    g_return_val_if_fail (DOCSET_IS_CURSOR(self), NULL);
    g_return_val_if_fail(pattern != NULL, NULL);

    DocsetEntry *entry;
    GList *entries = NULL;

    entry = docset_cursor_find (self, pattern);
    while (entry != NULL) {
        entries = g_list_prepend (entries, entry);
        entry = docset_cursor_next(self);
    }

    entries = g_list_reverse (entries);

    return entries;
}

DocsetCursor *
docset_cursor_new_for_file (GFile *db)
{
    g_return_val_if_fail(G_IS_FILE(db), NULL);

    DocsetCursor *self;
    DocsetCursorPrivate *priv;
    gint err = 0;
    gchar *db_path = NULL;

    db_path = g_file_get_path(db);
    self = g_object_new(DOCSET_TYPE_CURSOR, NULL);
    priv = docset_cursor_get_instance_private (self);
    err = sqlite3_open_v2 (db_path, &priv->db, SQLITE_OPEN_READONLY, NULL);
    if (err != SQLITE_OK || priv->db == NULL) {
        g_warning ("Failed to open sqlite3 database: %s\n",
                   sqlite3_errstr (err));
        g_clear_object(&self);
        goto exit;
    }

    priv->file = g_object_ref (db);
    priv->stmt = NULL;

exit:
    g_free (db_path);
    return self;
}

static void
docset_cursor_get_property (GObject *object,
                            guint property_id,
                            GValue *value,
                            GParamSpec *pspec)
{
    // DocsetCursor *self = DOCSET_CURSOR (object);
    // DocsetCursorPrivate *priv = docset_cursor_get_instance_private (self);

    switch (property_id) {
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        break;
    }
}

static void
docset_cursor_set_property (GObject *object,
                            guint property_id,
                            const GValue *value,
                            GParamSpec *pspec)
{
    // DocsetCursor *self = DOCSET_CURSOR (object);
    // DocsetCursorPrivate *priv = docset_cursor_get_instance_private (self);

    switch (property_id) {
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        break;
    }
}

static void
docset_cursor_dispose (GObject *object)
{
    DocsetCursor *self = DOCSET_CURSOR (object);
    DocsetCursorPrivate *priv = docset_cursor_get_instance_private (self);
    gint err = 0;

    err = sqlite3_finalize(priv->stmt);
    if (err != SQLITE_OK)
        g_warning ("Clearing database statement failed %d %s\n",
                err,
                sqlite3_errstr (err));

    err = sqlite3_close_v2(priv->db);
    if (err != SQLITE_OK)
        g_warning ("Closing database failed %d %s\n",
                err,
                sqlite3_errstr (err));

    g_clear_object (&priv->file);

    G_OBJECT_CLASS (docset_cursor_parent_class)->dispose (object);
}

static void
docset_cursor_finalize (GObject *object)
{
    DocsetCursor *self = DOCSET_CURSOR (object);
    DocsetCursorPrivate *priv = docset_cursor_get_instance_private (self);

    G_OBJECT_CLASS (docset_cursor_parent_class)->finalize (object);
}

static void
docset_cursor_constructed (GObject *object)
{
    // DocsetCursor *self = DOCSET_CURSOR (object);

    G_OBJECT_CLASS (docset_cursor_parent_class)->constructed (object);
}

static void
docset_cursor_class_init (DocsetCursorClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);

    object_class->get_property = docset_cursor_get_property;
    object_class->set_property = docset_cursor_set_property;
    object_class->constructed = docset_cursor_constructed;
    object_class->dispose = docset_cursor_dispose;
    object_class->finalize = docset_cursor_finalize;

    /* g_object_class_install_properties (object_class, N_PROPS,
     * obj_properties); */
}

static void
docset_cursor_init (DocsetCursor *self)
{
    self->priv = docset_cursor_get_instance_private (self);
}
