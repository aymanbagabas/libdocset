/*
 * SPDX-FileCopyrightText: 2020 Ayman Bagabas <ayman.bagabas@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "docset-entry.h"

/**
 * SECTION: docset-entry
 * @title: DocsetEntry
 * @short_description: TODO
 *
 * TODO
 */

/**
 * DocsetEntry:
 *
 * Data structure representing a #DocsetEntry.
 *
 * Since: UNRELEASED
 */

/**
 * DocsetEntryClass:
 *
 * The class of a #DocsetEntry.
 *
 * Since: UNRELEASED
 */

G_DEFINE_BOXED_TYPE (DocsetEntry, docset_entry, docset_entry_copy, docset_entry_free)

const gchar *
docset_entry_get_name (DocsetEntry *self)
{
    g_return_val_if_fail(self != NULL, NULL);

    return self->name;
}

const gchar *
docset_entry_get_path (DocsetEntry *self)
{
    g_return_val_if_fail(self != NULL, NULL);
    return self->path;
}

const DocsetEntryType *
docset_entry_get_entry_type (DocsetEntry *self)
{
    g_return_val_if_fail(self != NULL, NULL);
    return self->type;
}

DocsetEntryTypeId
docset_entry_get_type_id (DocsetEntry *self)
{
    g_return_val_if_fail(self != NULL, DOCSET_ENTRY_TYPE_ID_UNKNOWN);
    return self->type->id;
}

const gchar *
docset_entry_get_type_name (DocsetEntry *self)
{
    g_return_val_if_fail(self != NULL, NULL);
    return self->type->name;
}

DocsetEntry *
docset_entry_new (gint id,
                  const gchar *name,
                  const gchar *type,
                  const gchar *path)
{
    DocsetEntry *self;
    const DocsetEntryType *entry_type;
    GRegex *regex;
    gchar **matches;

    regex = g_regex_new("<.*>(.*)", 0, 0, NULL);
    self = g_slice_new0(DocsetEntry);
    entry_type = docset_entry_type_find (type);

    if (!entry_type ||
        !*entry_type->name ||
        entry_type->id == DOCSET_ENTRY_TYPE_ID_UNKNOWN) {
        g_warning("Unknown entry type detected '%s'.\n", type);
        entry_type = &docset_entry_type_unknown;
    }

    self->id = id;
    self->name = g_strdup (name);
    self->type = entry_type;
    if (g_regex_match(regex, path, 0, 0)) {
        matches = g_regex_split(regex, path, 0);
        self->path = g_strdup(matches[1]);
        g_strfreev(matches);
    } else {
        self->path = g_strdup (path);
    }

    g_regex_unref(regex);

    return self;
}

DocsetEntry *
docset_entry_copy (DocsetEntry *self)
{
    g_return_val_if_fail(self != NULL, NULL);

    DocsetEntry *entry;

    entry = g_slice_new0(DocsetEntry);
    *entry = *self;

    entry->id = self->id;
    entry->name = g_strdup (self->name);
    entry->type = self->type;
    entry->path = g_strdup (self->path);

    return entry;
}

void
docset_entry_free (DocsetEntry *self)
{
    g_return_if_fail(self != NULL);

    g_free (self->name);
    g_free (self->path);
    g_slice_free(DocsetEntry, self);
}
