/*
 * SPDX-FileCopyrightText: 2020 Ayman Bagabas <ayman.bagabas@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef __DOCSET_CURSOR_H__
#define __DOCSET_CURSOR_H__

#include <gio/gio.h>
#include <glib-object.h>
#include <sqlite3.h>
#include "docset-entry.h"

G_BEGIN_DECLS

typedef struct _DocsetCursor DocsetCursor;
typedef struct _DocsetCursorClass DocsetCursorClass;
typedef struct _DocsetCursorPrivate DocsetCursorPrivate;

#define DOCSET_TYPE_CURSOR (docset_cursor_get_type ())
#define DOCSET_CURSOR(obj)                                                    \
    (G_TYPE_CHECK_INSTANCE_CAST ((obj), DOCSET_TYPE_CURSOR, DocsetCursor))
#define DOCSET_CURSOR_CLASS(klass)                                            \
    (G_TYPE_CHECK_CLASS_CAST ((klass), DOCSET_TYPE_CURSOR, DocsetCursorClass))
#define DOCSET_IS_CURSOR(obj)                                                 \
    (G_TYPE_CHECK_INSTANCE_TYPE ((obj), DOCSET_TYPE_CURSOR))
#define DOCSET_IS_CURSOR_CLASS(klass)                                         \
    (G_TYPE_CHECK_CLASS_TYPE ((klass), DOCSET_TYPE_CURSOR))
#define DOCSET_CURSOR_GET_CLASS(obj)                                          \
    (G_TYPE_INSTANCE_GET_CLASS ((obj), DOCSET_TYPE_CURSOR, DocsetCursorClass))

struct _DocsetCursorClass {
    GObjectClass parent_class;

    /* Padding for future expansion */
    gpointer padding[12];
};

struct _DocsetCursor {
    GObject parent_instance;

    DocsetCursorPrivate *priv;
};

GType         docset_cursor_get_type      (void);

DocsetCursor *docset_cursor_new_for_file  (GFile *db);

DocsetEntry  *docset_cursor_find          (DocsetCursor *self,
                                           const gchar *pattern);

GList        *docset_cursor_vfind         (DocsetCursor *self,
                                           const gchar *pattern);

GList        *docset_cursor_get_entries   (DocsetCursor *self);

gint          docset_cursor_count_entries (DocsetCursor *self);

DocsetEntry  *docset_cursor_next          (DocsetCursor *self);

G_END_DECLS

#endif /* #ifndef __DOCSET_CURSOR_H__*/
