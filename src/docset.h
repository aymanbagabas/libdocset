/*
 * SPDX-FileCopyrightText: 2020 Ayman Bagabas <ayman.bagabas@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef __DOCSET_H__
#define __DOCSET_H__

#include <glib.h>

G_BEGIN_DECLS

#define DOCSET_INSIDE
#include "docset-cursor.h"
#include "docset-file.h"
#include "docset-entry.h"
#include "docset-entry-type.h"
#include "docset-version.h"
#include "docset-enum-types.h"
#undef DOCSET_INSIDE

G_END_DECLS

#endif /* #ifndef __DOCSET_H__*/
