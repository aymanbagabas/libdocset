#include "docset.h"
#include "docset-cursor.h"
#include "docset-entry-type.h"
#include "docset-entry.h"
#include <locale.h>
#include <stdio.h>

void
print_entry (DocsetEntry *entry)
{

    g_print ("ID: %d\nName: %s\nType: %s\nPath: %s\nEntryID: %d\n\n",
             entry->id,
             entry->name,
             entry->type->name,
             entry->path,
             entry->type->id);
}

void
fentry (gpointer data, gpointer user_data)
{
    DocsetEntry *entry;

    entry = data;
    print_entry (entry);
}

void
ghtest (gpointer key, gpointer value, gpointer user_data)
{
    g_print ("%s: %s\n", key, value);
}

int
main (int argc, char *argv[])
{
    /* if (argc != 3) { */
    /*     g_printerr ("Unknown usage\n"); */
    /*     return -1; */
    /* } */

    DocsetFile *file = NULL;
    gchar *path = NULL;

    path = g_strdup (argv[1]);
    file = docset_file_new_for_path (path);
    if (file == NULL) {
        g_print ("File is NULL\n");
        g_free (path);
        return -1;
    }
    g_free (path);

    DocsetCursor *cursor;

    cursor = docset_file_get_cursor (file);
    if (!cursor) {
        g_print ("No cursor\n");
        return -1;
    }

    gchar *pattern = NULL;

    pattern = g_strdup_printf ("%s", argv[2]);
    g_print ("Pattern: %s\n\n", pattern);
    DocsetEntry *entry = NULL;

    entry = docset_cursor_find (cursor, pattern);
    while (entry != NULL) {
        /* print_entry(entry); */
        docset_entry_free (entry);
        entry = docset_cursor_next (cursor);
    }

    GList *list;

    list = docset_cursor_vfind (cursor, pattern);
    g_list_foreach (list, (GFunc)fentry, NULL);
    g_print ("# of entries: %d\n\n", docset_cursor_count_entries (cursor));

    GHashTable *props;

    props = docset_file_get_props (file);
    g_hash_table_foreach (props, ghtest, NULL);

    g_print ("\nBundle id: %s\nBundle name: %s\nIs dash: %s\nIs JS: %s\n"
             "Platform: %s\nIndex: %s\n",
             docset_file_get_props_bundle_id (file),
             docset_file_get_props_bundle_name (file),
             docset_file_get_props_is_dash (file) ? "yes" : "no",
             docset_file_get_props_is_javascript_enabled (file) ? "yes" : "no",
             docset_file_get_props_platform_family (file),
             docset_file_get_props_index_filepath (file));

    gchar *root_path = docset_file_get_root_path     (file);
    gchar *doc_path = docset_file_get_document_path (file);
    gchar *index_path = docset_file_get_index_path    (file);
    g_print("\nroot: %s\ndoc: %s\nindex: %s\n",
            root_path,
            doc_path,
            index_path);

    g_free(root_path);
    g_free(doc_path);
    g_free(index_path);
    g_free (pattern);
    g_list_free_full (list, (GDestroyNotify)docset_entry_free);
    g_object_unref (file);

    return 0;
}
